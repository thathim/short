<?php $__env->startSection('content'); ?>
    <h1 class="p-3 mb-2 bg-info text-white"> CREATE - SHORT URL </h1>
    <form method="post" action="<?php echo e(url('/')); ?>">
        <?php echo csrf_field(); ?>
        <div class="form-group">
            <h4> LONG URL </h4>
            <input type="text" name="long_url" class="form-control">
        </div>

        <button class="btn btn-success" type="submit">  CREATE SHORT URL </button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/short2/resources/views/create.blade.php ENDPATH**/ ?>