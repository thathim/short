<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos = Todo::OrderBy('created_at', 'desc')->get();
        return view('index')->with('todos',$todos);
        //เอาไว้ส่งไปหน้า index
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // ให้ฟังก์ชันรีเทิร์นไปที่ view create (ที่มี input 1 ช่อง)
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //
        $charset = "abcdefghijklmnopqrstuvwxyz";
        $numset = "0123456789";
        $url = "";

        //วน for 5 รอบ เพราะเอาตัวเลข 5 ตัว
        for ($i=0; $i<5; $i++)
        {
            // .= คือ $url = $url + ...... เขียนย่อๆ
            // $url .= $numset[rand(0,9)]; แบบนี้ก็ได้ฮะ
            $url .= $numset[rand(0,strlen($numset))-1];
        }

        for ($i=0; $i<3; $i++)
        {
            $url .= $charset[rand(0,strlen($charset))-1];
            $this->validate($request,
                [
                    'long_url'=>'required',
                ]
            );
        }



        $todo = new Todo();
        $todo->long_url = $request->input('long_url');
        $todo->short_url = $url;
        $todo->view = 0;
        $todo->save();

//        return redirect('/new')->with('success','Success!');

        return redirect('/new')->with('success',
            'Success! http://short.local/'.$todo->short_url);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($shorturl)
    {
        //
        $todos = Todo::aLL();
        if(count($todos)>0){
            foreach ($todos as $todo){
                if($todo->short_url == $shorturl){
                    $todo->view += 1;
                    $todo->save();
                    return view('redirect')->with('longurl', $todo->long_url);
                }
            }
        }
        return view('notfound');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
