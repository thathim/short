@extends('layouts.app')
@section('content')
    <h1 class="p-3 mb-2 bg-info text-white"> CREATE - SHORT URL </h1>
    <form method="post" action="{{ url('/') }}">
        @csrf
        <div class="form-group">
            <h4> LONG URL </h4>
            <input type="text" name="long_url" class="form-control">
        </div>

        <button class="btn btn-success" type="submit">  CREATE SHORT URL </button>
    </form>
@endsection
