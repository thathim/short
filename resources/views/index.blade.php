@extends('layouts.app')
@section('content')
    <h1 class="p-3 mb-2 bg-info text-white mt-3"> LIST </h1>
    @if(count($todos)>0)
        @foreach($todos as $todo)
            <div>
                <p class="mt-5 text-success" > VIEW : {{$todo->view}}  </p>
                <p>{{ $todo->create_at }}
                <a href=" {{url($todo->long_url)}} ">
                <h4 class="text-warning"> {{$todo->long_url}} </h4>
                </a>
                <input id="shorturl{{$todo->id}}" class="form-control" type="text"
                       value="http://www.short.local/t/{{$todo->short_url}}" readonly>

                <button onclick="copy(this)" value="{{$todo->id}}" type="button" class="btn btn-warning" > COPY </button>

            </div>
        @endforeach
    @endif

    <script>
        function copy(clickedBtn) {
            var id = clickedBtn.value;
            var copyText = document.querySelector('#shorturl'+id);
            copyText.select();
            document.execCommand('copy');
            alert('Copied' + copyText.value);
        }
    </script>

@endsection
